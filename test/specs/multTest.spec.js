const Calculator = require('../../app/calculator');
const { expect } = require ('chai');

describe('multiplyTest scenarions', function(){
    let calc
    beforeEach(function(){
        calc = new Calculator()
    })
    afterEach(function(){
        calc = null;
    })

    it('shouls return mult of two double positive numbers', function(){
        expect(calc.multiply(4.4, 5.2)).to.be.equal(4.4*5.2)
    })
    it('shouls return sum of two integer positive numbers', function(){
        expect(calc.multiply(4, 5)).to.be.equal(4*5)
    })
    it('shouls return sum of two integer negative numbers', function(){
        expect(calc.multiply(-4, -5)).to.be.equal(-4*(-5))
    })
    it('shouls return sum of two double positive numbers', function(){
        expect(calc.multiply(-4.4, -5.2)).to.be.equal(-4.4*(-5.2))
    })
})